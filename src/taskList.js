import React from "react";
import { View, Text, TouchableOpacity, FlatList } from 'react-native';

const TaskList = ({ navigation }) => {

    const tasks = [{
        id: 1,
        title: 'Ir ao Mercado',
        date: '2024-02-27',
        time: '10:00',
        address: 'Super SS'
    }, {
        id: 2,
        title: 'Aula de Teatro',
        date: '2024-02-27',
        time: '18:00',
        address: 'LaDive'
    }, {
        id: 3,
        title: 'Estudar para o exame',
        date: '2024-02-28',
        time: '09:00',
        address: 'Em casa'
    }];

    const taskPress = (task) =>{
        navigation.navigate('DetalhesDasTarefas',  {task})
    };

    return (
        <View>
            <FlatList
                data={tasks}
                keyExtractor={(item) => item.id.toString}
                renderItem={({ item }) => (
                    <TouchableOpacity 
                    onPress={() => taskPress (item)}>
                        <Text>{item.title}</Text>
                    </TouchableOpacity>
                )} />
        </View>
    );
}
export default TaskList;